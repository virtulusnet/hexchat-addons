#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
## HexChat script that creates a menu of commands for the Pirates Game
# https://gitlab.com/virtulusnet/hexchat-addons/-/blob/master/pirates.py
#
## TODO
# add menu items for targets
#

from datetime import datetime, timedelta
from itertools import groupby
from time import sleep

import hexchat
from hexchat import command

__module_name__ = 'Pirates Menu'
__module_author__ = 'numbafive'
__module_version__ = '0.9'
__module_description__ = 'Creates list of pirates game commands'

#
## Options
#
use_bonus_logger = True

allow = {
    'network': "Virtulus",
    'chan':    "#pirates"
}

timezone = "EST"

tz_offset = {
    'PDT': -3,
    'MDT': -2,
    'CDT': -1,
    'EST':  0,
    'ADT':  1,
    'GMT':  4,
    'BST':  5,
    'CST':  6,
    'MSK':  7,
}

menu = "🏴‍☠️ Pirates"

#
## Names
#
ships = [
    'Dark Sails',
    'Mousey McMouseface',
    'Rising Sun',
    'The Elite',
    'The Flying Dragon',
    'The Robothive',
    'The Wandering Soul',
    'The Revenge (bot)',
    'HMS Interceptor (bot)'
]
ports = [
    'Antigua',
    'Barbados',
    'Bermuda',
    'Campeche',
    'Caracas',
    'Cartagena',
    'Coro',
    'Cumana',
    'Curacao',
    'Eluthera',
    'Florida Keys',
    'Gibraltar',
    'Gran Grenada',
    'Grand Bahama',
    'Guadeloupe',
    'Havana',
    'Leogane',
    'Maracaibo',
    'Margarita',
    'Martinique',
    'Montserrat',
    'Nassau',
    'Nevis',
    'Nombre de dios',
    'Panama',
    'Petit Goave',
    'Port de Paix',
    'Puerto Bello',
    'Puerto Cabello',
    'Puerto Principe',
    'Rio de la Hacha',
    'San Juan',
    'Santa Catalina',
    'Santa Marta',
    'Santiago de La Vega',
    'Santiago',
    'Santo Domingo',
    'Sint Maarten',
    'St. Augustine',
    'St. Eustatius',
    'St. Kitts',
    'St. Martin',
    'Tortuga',
    'Trinidad',
    'Vera Cruz',
    'Villa Hermosa'
]
daily = [
    'Catch',
    'Catch',
    'Charver',
    'Dig',
    'Duel',
    'Lockpick',
    'Mutiny',
    'Party',
    'Quest',
    'Rob',
    'Troll',
    'Wench',
    'Work'
]

#
## Events
#
Sunday = {
    'Sunday Funday': '5pm for a Battle-Royale',
    'Sloggin Sunday': '1% bonus for work commands',
    'Effects': 'Greater chance of success during quests, attacks, raids',
}
Monday = {
    'Swipe n Grab': 'Items found start the grab event',
    'Mayhem Monday': '1% fight bonus',
}
Tuesday = {
    'Rob the Captain': 'Rob the Captain to become the Captain',
    'Treasure Tuesday': '1% dig bonus',
}
Wednesday = {
    'A Decuple o Treasures': '10 Global treasures will spawn on the map',
    'Questin Wednesday': '1% bonus for personal quest, 4% bonus for a group quest',
}
Thursday = {
    'Speedy Sailin': 'All ships will sail at maximum speed',
    'Trawlin Thursday': '1% fish bonus',
}
Friday = {
    'Foray Friday': 'Chance to raid any port ye sail to',
    'Fish Friday': 'Fishin payout n limits be increased n fishin cooldown be halved',
    'Festivity Friday': '1% bonus for all games',
    'Effects': 'Greater chance of failure during quests, attacks, raids',
}
Saturday = {
    'Psycho Saturday': '8pm for a Battle-Royale',
    'Submerged Saturday': '1% dive bonus',
}


#
## Main
#
ctx = None
def get_context():
    global ctx
    if ctx: return ctx
    ctx = hexchat.find_context(server=allow['network'], channel=allow['chan'])
    return ctx if ctx else None


# Remove menu on script unload
def unload_cb(userdata):
    command(f'menu del "{menu}"')
    command(f'menu del "$NICK/{menu}"')
    print(f"{__module_name__} unloaded")


# Split a list into sublists by delimiter
def split_list(l):
    delim = ";"
    return [list(y) for x, y in groupby(l, lambda z: z == delim) if not x]


# Button callback
def pirate_cb(word, word_eol, userdata):
    # send command to pirates channel even if used elsewhere
    ctx = get_context()
    if not ctx:
        print(f"You need to join the {allow['chan']} channel first!")
        return hexchat.EAT_ALL

    cmds = split_list(word)
    j = len(cmds)
    for i in cmds:
        ctx.command(f"say {' '.join(i)}")
        # wait before sending multiple lines
        if j > 1:
            sleep(2)
            j -= 1
    return hexchat.EAT_ALL


def recv_notice_cb(word, word_eol, userdata):
    nick = word[0][1:].split('!')[0]
    #chan = word[2]
    if nick != "CaptainJack":
        return hexchat.EAT_NONE
    try:
        msg = hexchat.strip(word_eol[4], -1, 3)
    except:
        return hexchat.EAT_NONE

    if msg.startswith("[P]"):
        # first of the day bonus
        if msg.endswith("o' the day!"):
            for task in daily:
                if task in msg:
                    command(f'menu -e0 -t1 add "{menu}/Daily Bonus/{task}"')


def checktime_cb(userdata):
    # compare time now vs time before
    # reset daily bonus list when its a new day
    global start_time
    now = datetime.now() - timedelta(hours=tz_offset[timezone])
    if start_time.day != now.day:
        start_time = now
        # redraw menu items
        for task in daily:
            command(f'menu -e0 -t0 add "{menu}/Daily Bonus/{task}"')
        command(f'menu del "{menu}/Todays Events"')
        day_menu()
        print(f"Pirates daily bonus' reset")
    return True


def main_menu():
    command(f'menu -p0 add "{menu}"')
    command(f'menu add "{menu}/Player"')
    command(f'menu add "{menu}/Player/Skills"')
    command(f'menu add "{menu}/Player/Skills/Learn"')
    command(f'menu add "{menu}/Player/Skills/Learn/Charisma      +Scuttlebutt" "p skill start chrisma aye"')
    command(f'menu add "{menu}/Player/Skills/Learn/Defense       +duel power"  "p skill start defense aye"')
    command(f'menu add "{menu}/Player/Skills/Learn/Fishing       +Crabbin"     "p skill start fishing aye"')
    command(f'menu add "{menu}/Player/Skills/Learn/Luck          +Gamble"      "p skill start luck aye"')
    command(f'menu add "{menu}/Player/Skills/Learn/Marksman      +gun power"   "p skill start marksman aye"')
    command(f'menu add "{menu}/Player/Skills/Learn/Meditation    +idle bonus"  "p skill start meditation aye"')
    command(f'menu add "{menu}/Player/Skills/Learn/Swordsmanship +sword power" "p skill start swordsmanship aye"')
    command(f'menu add "{menu}/Player/Skills/Learn/Thievery      +Snatch"      "p skill start thievery aye"')
    command(f'menu add "{menu}/Player/Skills/-"')
    command(f'menu add "{menu}/Player/Skills/Crabbin"     "p crabbin"')
    command(f'menu add "{menu}/Player/Skills/Gamble"      "p gamble"')
    command(f'menu add "{menu}/Player/Skills/Scuttlebutt" "p scuttlebutt"')
    command(f'menu add "{menu}/Player/Skills/Snatch"      "p snatch"')
    command(f'menu add "{menu}/Player/Jobs"')
    command(f'menu add "{menu}/Player/Jobs/Carry"   "p carry"')
    command(f'menu add "{menu}/Player/Jobs/Clean"   "p clean"')
    command(f'menu add "{menu}/Player/Jobs/Lookout" "p lookout"')
    command(f'menu add "{menu}/Player/Jobs/Rig"     "p rig"')
    command(f'menu add "{menu}/Player/Jobs/Swab"    "p swab"')
    command(f'menu add "{menu}/Player/Jobs/-"')
    command(f'menu add "{menu}/Player/Jobs/Cook"    "p cook"')
    command(f'menu add "{menu}/Player/Jobs/Fix"     "p fix"')
    command(f'menu add "{menu}/Player/Jobs/Repair"  "p repair"')
    command(f'menu add "{menu}/Player/Jobs/Steer"   "p steer"')
    command(f'menu add "{menu}/Player/Jobs/-"')
    command(f'menu add "{menu}/Player/Jobs/Dig"     "p dig"')
    command(f'menu add "{menu}/Player/Jobs/Dive"    "p dive"')
    command(f'menu add "{menu}/Player/Jobs/Fish"    "p fish"')
    command(f'menu add "{menu}/Player/Jobs/Work"    "p work"')
    command(f'menu add "{menu}/Player/Quest"')
    command(f'menu -e0 add "{menu}/Player/Quest/  -Start-" ""')
    command(f'menu add "{menu}/Player/Quest/Bounty"   "p quests start bounty"')
    command(f'menu add "{menu}/Player/Quest/Item"     "p quests start item"')
    command(f'menu add "{menu}/Player/Quest/Monster"  "p quests start monster"')
    command(f'menu add "{menu}/Player/Quest/Thief"    "p quests start thief"')
    command(f'menu add "{menu}/Player/Quest/Treasure" "p quests start treasure"')
    command(f'menu add "{menu}/Player/Rewards"')
    command(f'menu add "{menu}/Player/Rewards/Doubloons"  "p reward doubloons aye"')
    command(f'menu add "{menu}/Player/Rewards/Ornamental" "p reward ornamental aye"')
    command(f'menu add "{menu}/Player/Rewards/Power"      "p reward power aye"')
    command(f'menu add "{menu}/Player/Rewards/Stamina"    "p reward stamina aye"')
    command(f'menu add "{menu}/Player/Rewards/Voucher"    "p reward voucher aye"')
    command(f'menu add "{menu}/Player/Rewards/Random"     "p reward random aye"')
    command(f'menu add "{menu}/Player/Whistle"')
    command(f'menu add "{menu}/Player/Whistle/Andy Griffith Show"    "p whistle The Andy Griffith Show"')
    command(f'menu add "{menu}/Player/Whistle/Bandolero"             "p whistle Bandolero"')
    command(f'menu add "{menu}/Player/Whistle/Centerfold"            "p whistle Centerfold"')
    command(f'menu add "{menu}/Player/Whistle/Dock of the Bay"       "p whistle Dock of the Bay"')
    command(f'menu add "{menu}/Player/Whistle/Dont Worry Be Happy"   "p whistle Don\'t Worry, Be Happy"')
    command(f'menu add "{menu}/Player/Whistle/Ed Edd And Eddy"       "p whistle Ed Edd And Eddy Theme"')
    command(f'menu add "{menu}/Player/Whistle/Fishin Hole"           "p whistle The Fishin Hole"')
    command(f'menu add "{menu}/Player/Whistle/Fistful Of Dollars"    "p whistle A Fistful\' Of Dollars"')
    command(f'menu add "{menu}/Player/Whistle/Good Life"             "p whistle Good Life"')
    command(f'menu add "{menu}/Player/Whistle/Here Comes The Rain"   "p whistle Here Comes The Rain, Baby"')
    command(f'menu add "{menu}/Player/Whistle/Home"                  "p whistle Home"')
    command(f'menu add "{menu}/Player/Whistle/Jealous"               "p whistle Jealous"')
    command(f'menu add "{menu}/Player/Whistle/Noah and the Whale"    "p whistle Noah and the Whale"')
    command(f'menu add "{menu}/Player/Whistle/Patience"              "p whistle Patience"')
    command(f'menu add "{menu}/Player/Whistle/Pumped up Kicks"       "p whistle Pumped up Kicks"')
    command(f'menu add "{menu}/Player/Whistle/River Kwai March"      "p whistle The River Kwai March"')
    command(f'menu add "{menu}/Player/Whistle/Sissyneck"             "p whistle Sissyneck"')
    command(f'menu add "{menu}/Player/Whistle/Tighten Up"            "p whistle Tighten Up"')
    command(f'menu add "{menu}/Player/Whistle/Twisted Nerve"         "p whistle Twisted Nerve"')
    command(f'menu add "{menu}/Player/Whistle/Walk Like An Egyptian" "p whistle Walk Like An Egyptian"')
    command(f'menu add "{menu}/Player/Whistle/Winds of Change"       "p whistle Winds of Change"')
    command(f'menu add "{menu}/Player/Whistle/Young Folks"           "p whistle Young Folks"')
    command(f'menu add "{menu}/Player/Whistle/Zazie on the Metro"    "p whistle The Zazie on the Metro"')
    command(f'menu add "{menu}/Player/Donate"')
    command(f'menu add "{menu}/Player/Donate/Dutch"   "p donate dutch aye"')
    command(f'menu add "{menu}/Player/Donate/English" "p donate english aye"')
    command(f'menu add "{menu}/Player/Donate/French"  "p donate french aye"')
    command(f'menu add "{menu}/Player/Donate/Spanish" "p donate spanish aye"')
    # command(f'menu add "{menu}/Player/Invest"')
    # command(f'menu add "{menu}/Player/Invest/" "p invest "')
    command(f'menu add "{menu}/Player/-"')
    command(f'menu add "{menu}/Player/Hate"  "p hate ; p hate"')
    command(f'menu add "{menu}/Player/Love"  "p love ; p love"')
    command(f'menu add "{menu}/Player/Heal"  "p heal aye"')
    command(f'menu add "{menu}/Player/Rest"  "p rest"')
    command(f'menu add "{menu}/Player/Rusty" "p rusty"')
    command(f'menu add "{menu}/Player/Party" "p party start"')
    command(f'menu add "{menu}/Player/Revenge" "p revenge"')
    command(f'menu add "{menu}/Player/-"')
    command(f'menu -e0 add "{menu}/Player/  -Cannon-" ""')
    command(f'menu add "{menu}/Player/Load"')
    command(f'menu add "{menu}/Player/Load/Exploding"   "p load exploding"')
    command(f'menu add "{menu}/Player/Load/Canister"    "p load canister')
    command(f'menu add "{menu}/Player/Load/Chain"       "p load chain')
    command(f'menu add "{menu}/Player/Load/Normal"      "p load round')
    command(f'menu add "{menu}/Player/Unload"')
    command(f'menu add "{menu}/Player/Unload/Exploding" "p load exploding"')
    command(f'menu add "{menu}/Player/Unload/Canister"  "p load canister')
    command(f'menu add "{menu}/Player/Unload/Chain"     "p load chain')
    command(f'menu add "{menu}/Player/Unload/Normal"    "p load round')
    command(f'menu add "{menu}/Player/-"')
    command(f'menu -e0 add "{menu}/Player/  -Crew-" ""')
    command(f'menu add "{menu}/Player/Clean"  "p crew clean start"')
    command(f'menu add "{menu}/Player/Grease" "p crew grease start"')
    command(f'menu add "{menu}/Player/Repair" "p crew repair start"')
    command(f'menu add "{menu}/Player/Sabotage"')
    for ship in ships:
        name = ship.replace(' (bot)', '')
        command(f'menu add "{menu}/Player/Sabotage/{ship}"')
        for action in ['Attack', 'Damage', 'Loot', 'Tar']:
            cmd = f"p sabotage {action.lower()} {name}"
            command(f'menu add "{menu}/Player/Sabotage/{ship}/{action}" "{cmd} ; {cmd}"')
    command(f'menu add "{menu}/Player/-"')
    command(f'menu add "{menu}/Player/Info"')
    command(f'menu add "{menu}/Player/Info/About"       "p about"')
    command(f'menu add "{menu}/Player/Info/Achievement" "p achievements"')
    command(f'menu add "{menu}/Player/Info/Age"         "p aye"')
    command(f'menu add "{menu}/Player/Info/Bio"         "p bio"')
    command(f'menu add "{menu}/Player/Info/Bounties"    "p bounties"')
    command(f'menu add "{menu}/Player/Info/Brig"        "p brig"')
    command(f'menu add "{menu}/Player/Info/Captain"     "p captain"')
    command(f'menu add "{menu}/Player/Info/Chest"       "p chest"')
    command(f'menu add "{menu}/Player/Info/Daily"       "p daily"')
    command(f'menu add "{menu}/Player/Info/Disease"     "p disease"')
    command(f'menu add "{menu}/Player/Info/Goals"       "p goals"')
    command(f'menu add "{menu}/Player/Info/Homeport"    "p homeport"')
    command(f'menu add "{menu}/Player/Info/Info"        "p info"')
    command(f'menu add "{menu}/Player/Info/Legends"     "p legends"')
    command(f'menu add "{menu}/Player/Info/Level"       "p level"')
    command(f'menu add "{menu}/Player/Info/Maps"        "p maps"')
    command(f'menu add "{menu}/Player/Info/Mental"      "p mental"')
    command(f'menu add "{menu}/Player/Info/Mercy"       "p mercy"')
    command(f'menu add "{menu}/Player/Info/Mutiny"      "p mutiny aye"')
    command(f'menu add "{menu}/Player/Info/Newest"      "p newest"')
    command(f'menu add "{menu}/Player/Info/Power"       "p power"')
    command(f'menu add "{menu}/Player/Info/Reputation"  "p rep"')
    command(f'menu add "{menu}/Player/Info/Stamina"     "p stamina"')
    command(f'menu add "{menu}/Player/Info/Status"      "p status"')
    command(f'menu add "{menu}/Player/Info/Tasks"       "p tasks"')
    command(f'menu add "{menu}/Player/Info/Vouchers"    "p voucher"')
    command(f'menu add "{menu}/Player/Info/Weapon"      "p weapons"')

    command(f'menu add "{menu}/Player/Ship Taunt"')
    for ship in ships:
        cmd = f"p ship taunt {ship.replace(' (bot)', '')}"
        command(f'menu add "{menu}/Player/Ship Taunt/{ship}" "{cmd}"')
    command(f'menu add "{menu}/Player/Ship Crew"        "p ship pirates"')
    command(f'menu add "{menu}/Player/Ship Health"      "p ship hp"')
    command(f'menu add "{menu}/Player/Ship Route"       "p ship route"')
    command(f'menu add "{menu}/Player/Ship Speed"       "p ship speed"')
    command(f'menu add "{menu}/Player/Ship Stats"       "p ship stats"')
    command(f'menu add "{menu}/Player/Ship Status"      "p ship status"')

    command(f'menu add "{menu}/Store"')
    command(f'menu -e0 add "{menu}/Store/  -Weapons-" ""')
    command(f'menu add "{menu}/Store/Gun"')
    command(f'menu add "{menu}/Store/Sword" ')
    command(f'menu add "{menu}/Store/Gun/Buy"      "p store buy gun"')
    command(f'menu add "{menu}/Store/Gun/Sell"     "p store sell gun"')
    command(f'menu add "{menu}/Store/Gun/Repair"   "p store buy repair gun"')
    command(f'menu add "{menu}/Store/Sword/Buy"    "p store buy sword"')
    command(f'menu add "{menu}/Store/Sword/Sell"   "p store sell sword"')
    command(f'menu add "{menu}/Store/Sword/Repair" "p store buy repair sword"')
    command(f'menu add "{menu}/Store/Blackpowder"  "p store buy blackpowder"')
    command(f'menu add "{menu}/Store/Sharpen"      "p store buy sharpen"')
    command(f'menu add "{menu}/Store/-"')
    command(f'menu -e0 add "{menu}/Store/  -Change-" ""')
    command(f'menu add "{menu}/Store/Faction"')
    command(f'menu add "{menu}/Store/Faction/Dutch"   "p store buy faction dutch"')
    command(f'menu add "{menu}/Store/Faction/English" "p store buy faction english"')
    command(f'menu add "{menu}/Store/Faction/French"  "p store buy faction french"')
    command(f'menu add "{menu}/Store/Faction/Jesuit"  "p store buy faction jesuit"')
    command(f'menu add "{menu}/Store/Faction/Native"  "p store buy faction native"')
    command(f'menu add "{menu}/Store/Faction/Spanish" "p store buy faction spanish"')
    command(f'menu add "{menu}/Store/Faction/Pirate"  "p store buy faction pirate"')
    command(f'menu add "{menu}/Store/Gender"')
    command(f'menu add "{menu}/Store/Gender/Male"    "p store buy gender male"')
    command(f'menu add "{menu}/Store/Gender/Female"  "p store buy gender female"')
    command(f'menu add "{menu}/Store/Gender/Unknown" "p store buy gender undisclosed"')
    command(f'menu add "{menu}/Store/Nickname"       "p store buy nickname change"')
    command(f'menu add "{menu}/Store/-"')
    command(f'menu -e0 add "{menu}/Store/  -Buy-" ""')
    command(f'menu add "{menu}/Store/Brandy  +stamina"           "p tavern brandy"')
    command(f'menu add "{menu}/Store/Bribe   +become captain"    "p store buy bribe"')
    command(f'menu add "{menu}/Store/Coffee  +sober up"          "p store buy coffee"')
    command(f'menu add "{menu}/Store/Lottery +doubloons"')
    command(f'menu add "{menu}/Store/Lottery +doubloons/x1"      "p store buy lottery auto"')
    command(f'menu add "{menu}/Store/Lottery +doubloons/x5"      "p store buy lottery auto 5"')
    command(f'menu add "{menu}/Store/Lottery +doubloons/x10"     "p store buy lottery auto 10"')
    command(f'menu add "{menu}/Store/Parlay  +untouchable"       "p store buy parlay"')
    command(f'menu add "{menu}/Store/Refill  +flask->tavern"     "p tavern flask refill"')
    command(f'menu add "{menu}/Store/Refill  +flask->store"      "p store buy refill"')
    command(f'menu add "{menu}/Store/Rum     +stamina"           "p store buy rum"')
    command(f'menu add "{menu}/Store/Spouse  +stamina"')
    command(f'menu add "{menu}/Store/Spouse  +stamina/Scallywag" "p store buy spouse scallywag"')
    command(f'menu add "{menu}/Store/Spouse  +stamina/Wench"     "p store buy spouse wench"')
    command(f'menu add "{menu}/Store/Wench   +stamina"           "p wench buy"')
    # command(f'menu add "{menu}/Store/Youth" "p store buy youth')
    command(f'menu add "{menu}/Store/-"')
    command(f'menu add "{menu}/Store/Sales"                      "p store sales"')

    command(f'menu add "{menu}/Market"')
    command(f'menu -e0 add "{menu}/Market/  -Herbs-" ""')
    command(f'menu add "{menu}/Market/Black  +cure zombie"   "p market buy black herb aye"')
    command(f'menu add "{menu}/Market/Blue   +cure diseases" "p market buy blue herb aye"')
    command(f'menu add "{menu}/Market/Brown  +power"         "p market buy brown herb aye"')
    command(f'menu add "{menu}/Market/Red    +power"         "p market buy red herb aye"')
    command(f'menu add "{menu}/Market/Green  +stamina"       "p market buy green herb aye"')
    command(f'menu add "{menu}/Market/Yellow +stamina"       "p market buy yellow herb aye"')
    command(f'menu add "{menu}/Market/-"')
    command(f'menu -e0 add "{menu}/Market/  -Poisons-" ""')
    command(f'menu add "{menu}/Market/Anzid       +power"          "p market buy poison anzib aye"')
    command(f'menu add "{menu}/Market/Furux       +power"          "p market buy poison furux aye"')
    command(f'menu add "{menu}/Market/Mosfungus   +power"          "p market buy poison mosfungus aye"')
    command(f'menu add "{menu}/Market/Nightbane   +power"          "p market buy poison nightbane aye"')
    command(f'menu add "{menu}/Market/Bokor       +pirate->zombie" "p market buy poison bokor aye"')
    command(f'menu add "{menu}/Market/Nightmother +pirate->vamp"   "p market buy poison nightmother aye"')
    command(f'menu add "{menu}/Market/-"')
    command(f'menu -e0 add "{menu}/Market/  -Coins-" ""')
    command(f'menu add "{menu}/Market/Cob      +pirate->brig"   "p market buy coin cob aye"')
    command(f'menu add "{menu}/Market/Denier   +parley"         "p market buy coin denier aye"')
    command(f'menu add "{menu}/Market/Ducatoon +store discount" "p market buy coin ducatoon aye"')
    command(f'menu add "{menu}/Market/Farthing +favor english"  "p market buy coin farthing aye"')
    command(f'menu add "{menu}/Market/-"')
    command(f'menu -e0 add "{menu}/Market/  -Curses-" ""')
    command(f'menu add "{menu}/Market/Decay       +pirate->age"     "p market buy curse decay aye"')
    command(f'menu add "{menu}/Market/Destruction +ship->damage"    "p market buy curse destruction aye"')
    command(f'menu add "{menu}/Market/Expel       +ship->expel"     "p market buy curse expel aye"')
    command(f'menu add "{menu}/Market/Imprison    +pirate->trap"    "p market buy curse imprisonment aye"')
    command(f'menu add "{menu}/Market/Plague      +pirate->disease" "p market buy curse plague aye"')
    command(f'menu add "{menu}/Market/Shift       +teleport"        "p market buy curse shift aye"')
    command(f'menu add "{menu}/Market/Zonbiness   +pirate->zombie"  "p market buy curse zonbiness aye"')
    command(f'menu add "{menu}/Market/-"')
    command(f'menu -e0 add "{menu}/Market/  -Elixir-" ""')
    command(f'menu add "{menu}/Market/Health +pirate->heal"    "p market buy elixir health aye"')
    command(f'menu add "{menu}/Market/Shadow +hide from crime" "p market buy elixir shadow aye"')

    command(f'menu add "{menu}/Captain"')
    command(f'menu add "{menu}/Captain/Sail"')
    for port in ports:
        cmd = f"p sail {port}"
        command(f'menu add "{menu}/Captain/Sail/{port}" "{cmd} ; {cmd}')
    command(f'menu add "{menu}/Captain/-"')
    command(f'menu add "{menu}/Captain/Crew Bonus"             "p capn crew bonus"')
    command(f'menu add "{menu}/Captain/Fight Bot"              "p fight CaptainJack"')
    command(f'menu add "{menu}/Captain/Find"                   "p capn find"')
    command(f'menu add "{menu}/Captain/Mass Find"              "p capn mass find"')
    command(f'menu add "{menu}/Captain/Monster"                "p capn monster"')
    command(f'menu add "{menu}/Captain/Rest"                   "p capn rest"')
    command(f'menu add "{menu}/Captain/Riddle"                 "p capn riddle"')
    command(f'menu add "{menu}/Captain/Rumor    +hide ship"    "p capn rumor aye"')
    command(f'menu add "{menu}/Captain/Share"                  "p capn share aye"')
    command(f'menu add "{menu}/Captain/Special"                "p capn special aye"')
    command(f'menu add "{menu}/Captain/Teleport +pirate haven" "p capn teleport aye"')
    command(f'menu add "{menu}/Captain/View Requests"          "p requests"')
    command(f'menu add "{menu}/Captain/-"')
    command(f'menu add "{menu}/Captain/Alert"   "p capn alert Haalp Haalp"')
    command(f'menu add "{menu}/Captain/Command" "p capn command"')
    command(f'menu add "{menu}/Captain/Employ"  "p capn employ"')
    command(f'menu add "{menu}/Captain/Info"')
    command(f'menu add "{menu}/Captain/Info/Favor"      "p capn favor"')
    command(f'menu add "{menu}/Captain/Info/Reputation" "p capn rep"')
    command(f'menu add "{menu}/Captain/Info/Stats"      "p capn stats"')

    command(f'menu add "{menu}/Shipwright"')
    command(f'menu add "{menu}/Shipwright/List" "p ship upgrades"')
    command(f'menu add "{menu}/Shipwright/-"')
    command(f'menu -e0 add "{menu}/Shipwright/  -Buy-" ""')
    command(f'menu add "{menu}/Shipwright/Upgrades"')
    command(f'menu add "{menu}/Shipwright/Upgrades/Repair Kit"')
    command(f'menu add "{menu}/Shipwright/Upgrades/Repair Kit/Small  +20% HP"        "p sw buy repair kit small"')
    command(f'menu add "{menu}/Shipwright/Upgrades/Repair Kit/Medium +40% HP"        "p sw buy repair kit medium"')
    command(f'menu add "{menu}/Shipwright/Upgrades/Repair Kit/Large  +60% HP"')
    command(f'menu add "{menu}/Shipwright/Upgrades/Repair Kit/Large  +60% HP/Buy"    "p sw buy repair kit large"')
    command(f'menu add "{menu}/Shipwright/Upgrades/Repair Kit/Large  +60% HP/Use"    "p ship use repair kit large"')
    command(f'menu add "{menu}/Shipwright/Upgrades/Teleport Stone"')
    command(f'menu add "{menu}/Shipwright/Upgrades/Teleport Stone/Buy"               "p sw buy teleport stone"')
    command(f'menu add "{menu}/Shipwright/Upgrades/Teleport Stone/Use"               "p ship use teleport"')
    command(f'menu add "{menu}/Shipwright/Upgrades/-"')
    command(f'menu add "{menu}/Shipwright/Upgrades/Bronze Cannons    +cannon damage" "p sw buy bronze cannons"')
    command(f'menu add "{menu}/Shipwright/Upgrades/Copper Plating    +ship speed"    "p sw buy copper plating"')
    command(f'menu add "{menu}/Shipwright/Upgrades/Cotton Sails      +ship speed"    "p sw buy cotton sails"')
    command(f'menu add "{menu}/Shipwright/Upgrades/Fine-Grain Powder +cannon damage" "p sw buy fine-grain powder"')
    command(f'menu add "{menu}/Shipwright/Upgrades/Harpoon           +stun monsters" "p sw buy harpoon"')
    command(f'menu add "{menu}/Shipwright/Ammo"')
    command(f'menu add "{menu}/Shipwright/Ammo/Exploding"')
    command(f'menu add "{menu}/Shipwright/Ammo/Exploding/x1"  "p sw buy exploding shot"')
    command(f'menu add "{menu}/Shipwright/Ammo/Exploding/x5"  "p sw buy exploding shot 5 aye"')
    command(f'menu add "{menu}/Shipwright/Ammo/Exploding/x10" "p sw buy exploding shot 10 aye"')
    command(f'menu add "{menu}/Shipwright/Ammo/Canister"')
    command(f'menu add "{menu}/Shipwright/Ammo/Canister/x1"   "p sw buy canister shot"')
    command(f'menu add "{menu}/Shipwright/Ammo/Canister/x5"   "p sw buy canister shot 5 aye"')
    command(f'menu add "{menu}/Shipwright/Ammo/Canister/x10"  "p sw buy canister shot 10 aye"')
    command(f'menu add "{menu}/Shipwright/Ammo/Chain"')
    command(f'menu add "{menu}/Shipwright/Ammo/Chain/x1"      "p sw buy chain shot"')
    command(f'menu add "{menu}/Shipwright/Ammo/Chain/x5"      "p sw buy chain shot 5 aye"')
    command(f'menu add "{menu}/Shipwright/Ammo/Chain/x10"     "p sw buy chain shot 10 aye"')
    command(f'menu add "{menu}/Shipwright/Clean"  "p sw buy clean"')
    command(f'menu add "{menu}/Shipwright/Grease" "p sw buy grease"')
    command(f'menu add "{menu}/Shipwright/Repair" "p sw buy repair"')
    command(f'menu add "{menu}/Shipwright/-"')
    command(f'menu add "{menu}/Shipwright/List Upgrades" "p ship upgrades"')
    command(f'menu add "{menu}/Shipwright/Show Ammo"   "p ship ammo"')
    command(f'menu add "{menu}/Shipwright/Cargo"  "p ship cargo"')

    command(f'menu add "{menu}/Ports"')
    command(f'menu add "{menu}/Ports/Info"')
    for port in ports:
        command(f'menu add "{menu}/Ports/Info/{port}" "p port info {port}"')

    command(f'menu add "{menu}/Misc"')
    command(f'menu add "{menu}/Misc/Auction"  "p ah list"')
    command(f'menu add "{menu}/Misc/Awake"    "p awake"')
    command(f'menu add "{menu}/Misc/Bot"      "p bot"')
    command(f'menu add "{menu}/Misc/Monsters" "p monsters"')
    command(f'menu add "{menu}/Misc/Moon"     "p moon"')
    command(f'menu add "{menu}/Misc/News"     "p news"')
    command(f'menu add "{menu}/Misc/Ping"     "p ping"')
    command(f'menu add "{menu}/Misc/Time"     "p time"')
    command(f'menu add "{menu}/Misc/Today"    "p today"')
    command(f'menu add "{menu}/Misc/Top"      "p top"')
    command(f'menu add "{menu}/Misc/Vampyres" "p vampyres"')
    command(f'menu add "{menu}/Misc/Weather"  "p weather"')
    command(f'menu add "{menu}/Misc/-"')
    command(f'menu add "{menu}/Misc/Games"')
    command(f'menu add "{menu}/Misc/Games/AMS"       "p game ams"')
    command(f'menu add "{menu}/Misc/Games/Bomb"      "p game bomb"')
    command(f'menu add "{menu}/Misc/Games/BlackJack" "p game bj"')
    command(f'menu add "{menu}/Misc/Games/Dice"      "p game dice"')
    command(f'menu add "{menu}/Misc/Games/Drinking"  "p game drink"')
    command(f'menu add "{menu}/Misc/Games/GetEven"   "p game geteven"')
    command(f'menu add "{menu}/Misc/Games/HiLo"      "p game hilo"')
    command(f'menu add "{menu}/Misc/Games/Roulette"  "p game roulette"')
    command(f'menu add "{menu}/Misc/Games/Scrabble"  "p game scrabble"')
    command(f'menu add "{menu}/Misc/Games/TicTacToe" "p game tictactoe"')
    command(f'menu add "{menu}/Misc/Games/War"       "p game war"')
    command(f'menu add "{menu}/Misc/-"')
    command(f'menu add "{menu}/Misc/Announcements" "p announcements"')
    command(f'menu add "{menu}/Misc/Bug Report"    "p bug"')
    command(f'menu add "{menu}/Misc/Options"')
    command(f'menu -e0 add "{menu}/Misc/Options/  -Toggle-" ""')
    command(f'menu add "{menu}/Misc/Options/Message Type"')
    command(f'menu add "{menu}/Misc/Options/Message Type/Private" "p options msgtype messages"')
    command(f'menu add "{menu}/Misc/Options/Message Type/Notice"  "p options msgtype notices"')
    command(f'menu add "{menu}/Misc/Options/Autosell"  "p options autosell')
    command(f'menu add "{menu}/Misc/Options/Highlight" "p options highlight')
    command(f'menu add "{menu}/Misc/Options/Reminders" "p options reminders')
    command(f'menu add "{menu}/Misc/Options/Tutorial"  "p options tutorial')

    if use_bonus_logger:
        command(f'menu add "{menu}/Daily Bonus"')
        for task in daily:
            command(f'menu -e0 -t0 add "{menu}/Daily Bonus/{task}"')


def target_menu():
    # create menu for actions against other players
    command(f'menu -p0 add "$NICK/{menu}"')
    command(f'menu add "$NICK/{menu}/Rob" "p rob %s"')
    command(f'menu add "$NICK/{menu}/Lockpick" "p lockpick %s"')
    command(f'menu add "$NICK/{menu}/Fight" "p fight %s"')
    command(f'menu add "$NICK/{menu}/Sex" "p sex %s"')
    command(f'menu add "$NICK/{menu}/DavyJones" "p dj %s"')


def day_menu():
    # get name of day
    day = start_time.strftime('%A')
    command(f'menu add "{menu}/Todays Events"')
    # loop over each days events dictionary
    for event, desc in globals().get(day).items():
        command(f'menu -e0 add "{menu}/Todays Events/  -{event}-" ""')
        command(f'menu add "{menu}/Todays Events/{desc}" ""')

#
## Hooks
#
hexchat.hook_command('p', pirate_cb)
hexchat.hook_unload(unload_cb)

if use_bonus_logger:
    hexchat.hook_server("NOTICE", recv_notice_cb)
    hexchat.hook_server("PRIVMSG", recv_privmsg_cb)
    hexchat.hook_timer(300000, checktime_cb)  # 5 minutes

# date object of right now
# used for reseting daily bonus logger and daily events
start_time = datetime.now() - timedelta(hours=tz_offset[timezone])

main_menu()
target_menu()
day_menu()

print(f"{__module_name__} v{__module_version__} loaded")
