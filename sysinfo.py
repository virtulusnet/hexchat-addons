#!/usr/bin/env python

## Windows
# https://visualstudio.microsoft.com/visual-cpp-build-tools
# msvc++ and win sdk
# pip install psutil wmi pypiwin32
## nvidia
# pip install py3nvml

## Linux
# pacman install python3-psutil util-linux mesa-utils

## RouterOS
# pip install librouteros

__module_name__ = 'sysinfo'
__module_author__ = 'numbafive'
__module_version__ = '1.0'
__module_description__ = 'outputs system information'

import platform
import psutil
import re
import sys


# windows config
gpu_num = 0
net_iface = 0

# linux config
# regex: matches /boot /snap*
ignore_mount = "boot$|snap"

# mikrotik routerOS api
router = {
    'host': '192.168.1.1',
    'user': 'admin',
    'pass': '123'
}


def get_size(bytes: float, decimals: int = 1) -> str:  # type: ignore[return]
    factor = 1024.0
    for unit in ["", "K", "M", "G", "T", "P"]:
        if bytes < factor:
            return f"{round(bytes, decimals)}14{unit}B" if decimals > 0 else f"{int(bytes)}14{unit}B"
        bytes /= factor


def uptime(seconds: float) -> str:
    if seconds < 1.0:
        return '∞'
    parts: list[str] = []
    for unit, div in (
        ('14w', 60 * 60 * 24 * 7),
        ('14d', 60 * 60 * 24),
        ('14h', 60 * 60),
        ('14m', 60),
        # ('14s', 1),
    ):
        amount, seconds = divmod(int(seconds), div)
        if amount > 0:
            parts.append(f"{amount}{unit}")
    return ' '.join(parts)


def _print(text: str):
    if HEXCHAT:
        hexchat.command(text)
    else:
        print(pnt_sub.sub('', text).replace('\002', '\033[1m').replace('\017', '\033[0m'))


def sysinfo(word, word_eol, userdata):
    # no arguments defaults to showing everything
    section = word[1] if len(word) > 1 else 'all'

    cpu_name_sub = re.compile(r' (Dual|Triple|Quad|Six|Eight|\d?\d)-.*Processor| CPU| @.*[Hh]z| \(R|TM|\)')
    os_info_sub = re.compile(r'Microsoft | Preview')
    network_name_sub = re.compile(r' (Controller|Family|Adapter)|Network | (#|_)\d+')

    if HEXCHAT and section in ('all', 'version', 'hexchat'):
        hexchat.command(f"say -Hexchat {hexchat.get_info('version')}-")

    if platform.system() == "Windows":
        # Vbox needs: VBoxManage setextradata "vm name" "VBoxInternal/Devices/pcbios/0/Config/DmiExposeMemoryTable" 1
        import wmi
        c = wmi.WMI()

        # needed for python 3.9 and below
        # or set PYTHONUTF8=1
        if not HEXCHAT:
            sys.stdout.reconfigure(encoding='utf-8')

        if section in ('all', 'os'):
            import ctypes
            # change return type from DWORD to LONGLONG otherwise windows will return a negative number after 49.7 days
            GetTickCount = ctypes.windll.kernel32.GetTickCount64
            GetTickCount.restype = ctypes.c_ulonglong
            os_info = c.Win32_OperatingSystem()[0]
            os_name = os_info_sub.sub('', os_info.Caption)
            header = "    os" if section == 'all' else "os"
            _print(f"say {header}: {os_name} Build {os_info.BuildNumber} uptime: {uptime(GetTickCount()/1000.0)}")

        if section in ('mobo', 'motherboard'):
            motherboard = c.Win32_BaseBoard()[0].Product
            header = "  mobo" if section == 'all' else "mobo"
            _print(f"say {header}: {motherboard}")

        if section in ('all', 'cpu'):
            cpu_info = c.Win32_Processor()[0]
            cpu_name = cpu_name_sub.sub('', cpu_info.Name.strip())
            header = "   cpu" if section == 'all' else "cpu"
            _print(f"say {header}: {cpu_name} {cpu_info.NumberOfCores}C/{cpu_info.NumberOfLogicalProcessors}T • {cpu_info.MaxClockSpeed/1000:.2f}14GHz")

        if section in ('all', 'gpu'):
            gpu_info = c.Win32_VideoController()[0]
            # TODO - wmi cant show more than 4gb vram cuz values are 32bit
            # if gpu_info.AdapterCompatibility == "NVIDIA":
            #     # https://py3nvml.readthedocs.io/en/latest
            #     from py3nvml import py3nvml
            #     nvmlDeviceGetMemoryInfo()
            resolution = gpu_info.VideoModeDescription.split(' x ')
            header = "   gpu" if section == 'all' else "gpu"
            _print(f"say {header}: {gpu_info.Name} {get_size(abs(gpu_info.AdapterRAM), 0)} • {resolution[0]}14x{resolution[1]}")

        if section in ('all', 'memory', 'mem'):
            memory = c.Win32_PhysicalMemory()[gpu_num]
            mem = psutil.virtual_memory()
            header = "   mem" if section == 'all' else "mem"
            _print(f"say {header}: {get_size(mem.used)}/{get_size(mem.total)} used • {memory.Speed/2:.0f}14MHz")

        if section in ('all', 'disk', 'hdd', 'drives'):
            num = cap = use = 0
            partitions = psutil.disk_partitions()
            for volume in partitions:
                # prevents reading from empty drives such as cd-roms, card readers etc.
                if volume.fstype:
                    try:
                        stat = psutil.disk_usage(volume.device)
                        cap += stat.total
                        use += stat.used
                        num += 1
                    except (OSError, PermissionError):
                        pass
            header = "  disk" if section == 'all' else "disk"
            _print(f"say {header}: {num} volumes with {get_size(use)}/{get_size(int(cap))} used")

        if section in ('all', 'network', 'net'):
            # total tx/rx
            net_io = psutil.net_io_counters()
            # name and current tx/rx rate
            net_io_curr = c.Win32_PerfFormattedData_Tcpip_NetworkInterface()[net_iface]
            adapter = network_name_sub.sub('', net_io_curr.Name)
            header = "   net" if section == 'all' else "net"
            _print(f"say {header}: {adapter} ▲{get_size(net_io.bytes_sent)} {get_size(int(net_io_curr.BytesSentPersec))}/s • {get_size(net_io.bytes_recv)}▼ {get_size(int(net_io_curr.BytesReceivedPersec))}/s")

    else:
        # Linux
        import subprocess
        import time

        gpu_name = subprocess.run(["glxinfo", "-B"], text=True, stdout=subprocess.PIPE).stdout
        try:
            gpu_name = [x.split(':')[1] for x in gpu_name.split('\n') if "OpenGL renderer string" in x][0].strip()
        except IndexError:
            gpu_name = "None"

        if section in ('all', 'os'):
            os_info = platform.uname()
            header = "    os" if section == 'all' else "os"
            _print(f"say {header}: {os_info.system} {os_info.release}  uptime: {uptime(time.monotonic())}")

        if section in ('all', 'cpu'):
            freq = psutil.cpu_freq()
            cores = psutil.cpu_count(logical=False)
            threads = psutil.cpu_count()

            cpu_name = subprocess.run(["lscpu"], text=True, stdout=subprocess.PIPE).stdout
            try:
                cpu_name = [x.split(':')[1] for x in cpu_name.split('\n') if "Model name:" in x][-1].strip()
                cpu_name = cpu_name_sub.sub('', cpu_name)
            except IndexError:
                cpu_name = None

            # try to parse ARM cpu model
            if not cpu_name or cpu_name.startswith("Cortex"):
                try:
                    with open("/proc/device-tree/compatible", "r") as f:
                        cpu_name = f.readline().strip('\x00').split(',')[-1]
                except:
                    cpu_name = "unknown"

            header = "   cpu" if section == 'all' else "cpu"
            _print(f"say {header}: {cpu_name} {cores}C/{threads}T • {freq.current:.0f}14MHz/{freq.max:.0f}14MHz  gpu: {gpu_name}")

        if section in ('gpu'):
            header = "   gpu" if section == 'all' else "gpu"
            _print(f"say {header}: {gpu_name}")

        if section in ('all', 'memory', 'mem'):
            mem = psutil.virtual_memory()
            cur_load = psutil.getloadavg()
            header = "   mem" if section == 'all' else "mem"
            _print(f"say {header}: {get_size(mem.used-mem.slab-mem.buffers)}/{get_size(mem.total)} used  load: {cur_load[1]:.1f}14 5min • {cur_load[2]:.1f}14 15min")

        if section in ('all', 'disk', 'hdd', 'drives'):
            partitions = psutil.disk_partitions()
            for f in partitions:
                # exclude these partitions
                if re.search(rf"/({ignore_mount})", f.mountpoint):
                    continue
                try:
                    mount = psutil.disk_usage(f.mountpoint)
                except PermissionError:
                    continue

                header = "  disk" if section == 'all' else "disk"
                _print(f"say {header}: {f.device.replace('/dev/', '')} {f.fstype} • {get_size(mount.used)}/{get_size(mount.total)} {mount.percent}14% used")

        if section in ('all', 'temp'):
            # only show temps if they exist
            if hasattr(psutil, "sensors_temperatures"):
                temp = psutil.sensors_temperatures()
                temps = ""
                for t in temp:
                    temps += f"{t.replace('_thermal', '')} {round(temp[t][0].current)}14°C • "

                header = "  temp" if section == 'all' else "temp"
                _print(f"say {header}: {temps[:len(temps)-3]}")

        if section in ('all', 'network', 'net'):
            net_io = psutil.net_io_counters()
            time.sleep(1)
            net_sent, net_recv = net_io.bytes_sent, net_io.bytes_recv
            header = "   net" if section == 'all' else "net"
            _print(f"say {header}: ▲{get_size(net_sent)} {get_size(net_io.bytes_sent-net_sent)}/s • {get_size(net_recv)}▼ {get_size(net_io.bytes_recv-net_recv)}/s")

    # try:
    #     from ros import Ros
    #     from requests.packages import urllib3
    #     urllib3.disable_warnings()
    #     ros = Ros("https://192.168.1.1", "admin", "123")
    #     model = ros.system.routerboard.model.split("+")[0]
    #     firmware = ros.system.routerboard.current_firmware
    #     voltage = ros.system.health[0].value
    #     temp = ros.system.health[1].value
    #     _print(f"say router: {model} on 14v{firmware} • {temp}14°C @ {voltage} 14volts")
    # except:
    #     pass

    if section in ('all', 'router'):
        try:
            from librouteros import connect
            api = connect(host=router['host'], username=router['user'], password=router['pass'])
            health = tuple(api(cmd='/system/health/print'))
            voltage, temp = health[0]['value'], health[1]['value']
            resource = tuple(api(cmd='/system/resource/print'))[0]
            model, firmware = resource['board-name'].split("+")[0], resource['version'].split(" ")[0]
            # up = resource['uptime']
            mem_total, mem_free = resource['total-memory'], resource['free-memory']
            load = resource['cpu-load']
            _print(f"say router: {model} {firmware} • {load}% load • {get_size(mem_total - mem_free)}/{get_size(mem_total)} mem • {temp}14°C @ {voltage}14V")
        except:
            pass

    if HEXCHAT:
        # dont foward this command to other clients on the same bouncer
        return hexchat.EAT_ALL


def unload_cb(userdata):
    print(f"{__module_name__} unloaded.")


# strip control codes from console output
pnt_sub = re.compile(r'say |\026|\035|\036|\037|\003\d\d')

try:
    import hexchat
    hexchat.hook_command('sysinfo', sysinfo, help="Usage: /sysinfo")
    hexchat.hook_unload(unload_cb)
    print(f"{__module_name__} version {__module_version__} loaded.")
    HEXCHAT = True

except:
    HEXCHAT = False
    sysinfo(sys.argv, 0, 0)
