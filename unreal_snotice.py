import re
import hexchat

__module_name__ = 'Better unreal snotices'
__module_author__ = 'numbafive'
__module_version__ = '1.0'
__module_description__ = 'Remove junk from unreal-6'

#
## Options
#
networks = ["Virtulus"]

#
## Main
#
def recv_snotice(word, word_eol, userdata):
    if hexchat.get_info("network") in networks:
        msg = word[0]

        if "netsplit.de" in msg:
            return hexchat.EAT_HEXCHAT

        msg = re.sub(r"\[info\]", "🖥", hexchat.strip(msg))
        msg = re.sub(r"\[error\]", "04⚠   ", msg)

        if "Client connecting" in msg:
            msg = re.sub(r" Client (connect)ing:", r"09--▶ 14\1:", msg)
            msg = re.sub(r"\[(vhost: .*\.IP|secure: .*(POLY|SHA)\d+|reputation: \d+|security-groups: .*)\] ", "", msg)

        elif "Client exiting" in msg:
            msg = re.sub(r"Client (exiting):", r"08◀--14\1:", msg)

        else:
            msg = re.sub(r"([A-Z]-Line) added:", r"04!!! \1:", msg)
            msg = re.sub(r"(Oper(Override|Serv)):", r"07\1:", msg)
            msg = re.sub(r"(Warning:)", r"13\1", msg)

        hexchat.emit_print("Server Text", msg)
        return hexchat.EAT_HEXCHAT

#
## Hooks
#
hexchat.hook_print("Server Notice", recv_snotice)
print(f"{__module_name__} v{__module_version__} loaded")
