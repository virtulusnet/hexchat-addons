#!/usr/bin/python3
import re
import sys

dictionary = [
"abandon",
"adventure",
"affluent",
"anchor",
"antigua",
"apprentice",
"armada",
"ashore",
"assault",
"attack",
"aye-aye",
"ballast",
"bandanna",
"bandit",
"bandolier",
"barbados",
"barbaric",
"barkadeer",
"barque",
"barrel",
"battle",
"beggar",
"behead",
"bermuda",
"bilge",
"blackbeard",
"blimey",
"boatswain",
"boomin'",
"booty",
"bos'n",
"bounty",
"bowsprit",
"brawl",
"brethren",
"brigantine",
"broadside",
"brutal",
"buccaneer",
"bucko",
"bunghole",
"business",
"campeche",
"candlemas",
"cannon",
"capsize",
"capstan",
"captain",
"captaincy",
"capture",
"caracas",
"careen",
"caribbean",
"carouser",
"cartagena",
"challenge",
"chandler",
"chantey",
"cheater",
"clipper",
"coastline",
"coffers",
"commander",
"commands",
"compass",
"confiscate",
"conquest",
"contraband",
"corpse",
"corsair",
"corsairs",
"course",
"coxswain",
"criminal",
"cumana",
"curacao",
"curses",
"cutlass",
"cutthroat",
"dagger",
"danger",
"daring",
"deckhands",
"decuple",
"destitude",
"diamonds",
"diseases",
"dishonest",
"doubloons",
"draught",
"dutch",
"earring",
"eluthera",
"emeralds",
"english",
"escape",
"explore",
"eyepatch",
"factions",
"fathom",
"fearsome",
"ferocious",
"floggin'",
"flotsam",
"forecastle",
"fortune",
"french",
"galleon",
"galley",
"gangplank",
"gangway",
"gibbet",
"gibraltar",
"grapnel",
"guadeloupe",
"gunner",
"gunpowder",
"gunwalls",
"hands",
"hardtack",
"havana",
"hearties",
"heave",
"heist",
"hijack",
"hogshead",
"holystone",
"homeport",
"horizon",
"hornswaggler",
"hostile",
"hurricane",
"ill-gotten",
"illegal",
"impoverished",
"infamous",
"interloper",
"investments",
"island",
"jesuit",
"jetsam",
"jewels",
"keelhaul",
"kidnap",
"killick",
"knife",
"land-ho",
"landlubber",
"lanyard",
"lassie",
"lawless",
"legend",
"leogane",
"limey",
"lockpickin'",
"lookout",
"lottery",
"lucre",
"maggot",
"malaria",
"man-o-war",
"maracaibo",
"marauder",
"margarita",
"mariner",
"maroon",
"marooned",
"martinique",
"martinmas",
"master",
"mates",
"matey",
"mayhem",
"menace",
"merchant",
"minigames",
"montserrat",
"musket",
"mutineers",
"mutiny",
"nassau",
"native",
"nautical",
"navigate",
"nipperkin",
"notorious",
"opulent",
"outcasts",
"overboard",
"panama",
"parley",
"parrot",
"pegle",
"pillage",
"pirates",
"pistol",
"pistol-whip",
"plank",
"plunder",
"poopdeck",
"port-de-paix",
"predatory",
"pressgang",
"privateer",
"privateers",
"provost",
"prowl",
"quarter",
"quarterdesk",
"quartermaster",
"quarters",
"ransack",
"rations",
"realm",
"reckoning",
"referrals",
"revenge",
"revolt",
"riches",
"rigging",
"rudder",
"ruffian",
"ruthless",
"sabbath",
"sabotage",
"sailing",
"sailor",
"salmagundi",
"santiago",
"sapphires",
"scalawag",
"scallywag",
"schooner",
"scourge",
"scurvy",
"seadog",
"season",
"seaweed",
"secrets",
"sextant",
"shipmate",
"shipshape",
"silver",
"skiff",
"smugglers",
"spanish",
"spoils",
"sponsor",
"spyglass",
"square-rigged",
"squiffy",
"stamina",
"starboard",
"surrender",
"swabby",
"swagger",
"swashbuckling",
"teleport",
"thievery",
"topgallant",
"tortuga",
"torture",
"transom",
"treachery",
"treasure",
"trinidad",
"trollin'",
"unlawful",
"unscramble",
"unscrupulous",
"vampyr",
"vandalize",
"vanquish",
"vessels",
"vicious",
"villains",
"weapons",
"wenches",
"yardarm",
"yellow",
"yo-ho-ho",
"zonbi",
"Antigua",
"Barbados",
"Bermuda",
"Campeche",
"Caracas",
"Cartagena",
"Coro",
"Cumana",
"Curacao",
"Eluthera",
"Gibraltar",
"Guadeloupe",
"Havana",
"Leogane",
"Maracaibo",
"Margarita",
"Martinique",
"Montserrat",
"Nassau",
"Nevis",
"Panama",
"Port-de-Paix",
"Santiago",
"Tortuga",
"Trinidad",
]


def unscramble(scrambledWord):
    countToMatch = len(scrambledWord)
    matchedWords = []
    string = ""

    for word in dictionary:
        count = 0
        for x in scrambledWord:
            if x in word:
                count += 1
            if count == countToMatch:
                matchedWords.append(word)
                break
    for matchedWord in matchedWords:
        if len(matchedWord) == countToMatch:
            return matchedWord


def main(word):
    word = re.sub(r'[^a-zA-Z\'-]+', '', word, re.UNICODE)
    obscured = dict(sorted({i: word.count(i) for i in set(word)}.items(), key=lambda val: val[1], reverse=True))
    if list(obscured.values())[0] >= 5:
        word = word.replace(next(iter(obscured)), '')
    word = unscramble(word)
    if word is not None:
        print(unscramble(word))
    else:
        print("Nothing found")


main(sys.argv[1])
